const productModel = require('../models/productModel');

class ProductController {
  getProducts(req, res) {
    productModel.getProducts(result => {
      const { success, msg } = result;

      if (!success) {
        return res.status(404).send('Not Found');
      } else {
        return res.status(200).send(msg);
      }
    });
  };
}

module.exports = new ProductController();
