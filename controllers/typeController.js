const typeModel = require("../models/typeModel");

class TypeController {
  getAllTypes(req, res) {

    typeModel.getTypes(result => {
      const { success, msg } = result;

      if (!success || msg.length === 0) {
        return res.status(404).send('Not Found');
      } else {
        return res.status(200).send(msg);
      }
    });
  };
}

module.exports = new TypeController();
