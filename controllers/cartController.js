const cartModel = require('../models/cartModel');

class CartController {
  getCartItemsByClientId(req, res) {
    const { id } = req.params;

    cartModel.getCartItemsByClientId(id, result => {
      const { msg } = result;

      res.send(msg)
    });
  };

  addCartItem(req, res) {
    const {
      body: { id_client, id_tshirt, id_option, count },
    } = req;

    cartModel.addCartItem(id_client, id_tshirt, id_option, count, result => {
      const { success, msg } = result;

      if (!success) {
        return res.send({ msg });
      } else {
        return res.status(200).send({ msg: 'Changed successfully' });
      }
    });
  };

  deleteCartItem(req, res) {
    const {
      body: { id_client, id_tshirt, id_option },
    } = req;

    cartModel.deleteCartItem(id_client, id_tshirt, id_option, result => {
      const { success, msg } = result;

      if (!success) {
        return res.send({ msg });
      } else {
        return res.status(200).send({ msg: 'Removed successfully' });
      }
    });
  };
}

module.exports = new CartController();
