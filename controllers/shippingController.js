const shippingModel = require('../models/shippingModel');

class ShippingController {
  getShippingById(req, res) {
    const { id } = req.params;

    shippingModel.getShippingById(id, result => {
      const { success, msg } = result;

      if (!success || msg.length === 0) {
        return res.status(404).send('Not Found');
      } else {
        return res.status(200).send(msg);
      }
    });
  };

  addShippingOrder(req, res) {
    const {
      body: { type, price, status },
    } = req;

    shippingModel.addShipping(type, price, status, result => {
      res.send({
        msg: 'Shipping Successfully added'
      })
    });
  };
}

module.exports = new ShippingController();
