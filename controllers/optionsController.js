const optionsModel = require('../models/optionsModel');

class OptionsController {
  getOptionById(req, res) {
    const { id } = req.params;

    optionsModel.getOption(id, result => {
      const { success, msg } = result;

      if (!success) {
        return res.status(404).send('Not Found');
      } else {
        return res.status(200).send(msg);
      }
    });
  };

  getOptionId(req, res) {
    const {
      body: { id_material, id_product, id_type, id_color, id_size },
    } = req;

    optionsModel.getIdOption(id_material, id_product, id_type, id_color, id_size, result => {
      const { msg } = result;
      return res.send({ msg });
    });
  };

  getAllOptions(req, res) {
    optionsModel.getAllOptions(result => {
      const { success, msg } = result;

      if (!success || msg.length === 0) {
        return res.status(404).send('Not Found');
      } else {
        return res.status(200).send(msg);
      }
    });
  };

  addOption(req, res) {
    const {
      body: { id_material, id_product, id_type, id_color, id_size },
    } = req;

    optionsModel.addOneOption(id_material, id_product, id_type, id_color, id_size, result => {
      const { success, msg } = result;

      if (!success) {
        res.send({ msg })
      } else {
        res.send({
          msg: 'Option added'
        }
        );
      }
    });
  }
}

module.exports = new OptionsController();
