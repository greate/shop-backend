const materialModel = require("../models/materialModel");

class MaterialController {
  getMaterials(req, res) {

    materialModel.getMaterials(result => {
      const { success, msg } = result;

      if (!success) {
        return res.status(404).send('Not Found');
      } else {
        return res.status(200).send(msg);
      }
    });
  };
}

module.exports = new MaterialController();
