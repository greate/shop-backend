const orderModel = require('../models/orderModel');

class OrderController {
    create(req, res) {
        const {
            body: { id_client, id_payment, id_delivery, total_price },
        } = req;

        orderModel.createOrder(id_client, id_payment, id_delivery, total_price, orderResult => {
            orderModel.moveCartToOrder(orderResult.msg.insertId, id_client, cartResult => {
                res.send({
                    msg: orderResult.msg.insertId
                });
            })
        });
    }
}

module.exports = new OrderController();
