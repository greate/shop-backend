const categoryModel = require('../models/categoryModel');

class CategoryController {
  getCategoriesById(req, res) {
    const { id } = req.params;

    categoryModel.getCategoryById(id, result => {
      const { success, msg } = result;

      if (!success) {
        return res.status(404).send('Not Found');
      } else {
        return res.status(200).send(msg);
      }
    });
  };
}

module.exports = new CategoryController();
