const sizeModel = require("../models/sizeModel");

class SizeController {
  getAllSizes(req, res) {
    sizeModel.getSizes(result => {
      const { success, msg } = result;

      if (!success || msg.length === 0) {
        return res.status(404).send('Not Found');
      } else {
        return res.status(200).send(msg);
      }
    });
  };
}

module.exports = new SizeController();
