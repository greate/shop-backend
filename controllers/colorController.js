const colorModel = require('../models/colorModel');

class ColorController {
  getColors(req, res) {

    colorModel.getColors(result => {
      const { success, msg } = result;

      if (!success) {
        return res.status(404).send('Not Found');
      } else {
        return res.status(200).send(msg);
      }
    });
  };
}

module.exports = new ColorController();
