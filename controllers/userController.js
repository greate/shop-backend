const userModel = require('../models/userModel');

class UserController {
  register(req, res) {
    const {
      body: { name, address, phone, email, password },
    } = req;

    userModel.register(name, address, phone, email, password, result => {

      const { success, msg } = result;

      if (!success) {
        res.send({ msg })
      } else {
        res.send({
          message: 'Successful signUp'
        }
        );
      }
    });
  }

  login(req, res) {
    const { email, password } = req.body;
    const { method } = req;

    if (method === 'GET') return res.render('pages/login.hbs', {
      token: req.cookies.token
    });

    userModel.loginPromised(email, password, result => {
      const { success, msg } = result;

      res.send({
        success, msg
      });
    });
  }

  getUserByEmail(req, res) {
    const { email } = req.params;

    userModel.getUserByEmail(email, result => {
      res.send(result)
    })
  }
}

module.exports = new UserController();