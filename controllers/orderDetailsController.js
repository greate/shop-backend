const orderDetailsModel = require('../models/orderDetailsModel');

class OrderDetailsController {
  getAllOrderDetailsById(req, res) {
    const { id } = req.params;

    orderDetailsModel.getAllOrderDetailsById(id, result => {
      const { success, msg } = result;

      if (!success || msg.length === 0) {
        return res.status(404).send('Not Found');
      } else {
        return res.status(200).send(msg);
      }

    });
  };

  addOrderDetailsToTable(req, res) {
    const {
      body: { id_client, id_tshirt, id_option, count, price },
    } = req;

    orderDetailsModel.addOrderDetails(id_client, id_tshirt, id_option, count, price, result => {
      res.send(result)
    });
  };
}

module.exports = new OrderDetailsController();
