const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const {
  userRouter, categoryRouter, shippingRouter,
  optionsRouter, orderDetailsRouter,
  orderRouter, cartRouter, colorRouter,
  typeRouter, sizeRouter, materialRouter,
  productRouter, paymentRouter
} = require('./routes');

const app = express();

const urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(cors());
app.use(urlencodedParser);
app.use(cookieParser());

app.use('/api/user', userRouter);
app.use('/api/category', categoryRouter);
app.use('/api/shipping', shippingRouter);
app.use('/api/options', optionsRouter);
app.use('/api/order-details', orderDetailsRouter);
app.use('/api/order', orderRouter);
app.use('/api/cart', cartRouter);
app.use('/api/color', colorRouter);
app.use('/api/type', typeRouter);
app.use('/api/size', sizeRouter);
app.use('/api/material', materialRouter);
app.use('/api/products', productRouter);
app.use('/api/payment', paymentRouter);

app.listen(process.env.PORT || 3010, () => {
  console.log('Server started!');
});
