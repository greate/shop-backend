-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 11, 2021 at 04:55 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `t-short_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `Cart`
--

CREATE TABLE `Cart` (
  `id_cart` int(10) NOT NULL,
  `id_client` int(10) NOT NULL,
  `id_tshirt` int(10) NOT NULL,
  `id_option` int(10) NOT NULL,
  `count` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Cart`
--

INSERT INTO `Cart` (`id_cart`, `id_client`, `id_tshirt`, `id_option`, `count`) VALUES
(1, 1, 1, 2, 48),
(2, 2, 1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `Category`
--

CREATE TABLE `Category` (
  `id_category` int(10) NOT NULL,
  `id_tshirt` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Category`
--

INSERT INTO `Category` (`id_category`, `id_tshirt`, `name`) VALUES
(1, 1, 'Auto, Moto'),
(2, 2, 'IT'),
(3, 2, 'Internet Fun'),
(4, 1, 'Music'),
(5, 1, 'Holiday'),
(6, 1, 'Humor');

-- --------------------------------------------------------

--
-- Table structure for table `Clients`
--

CREATE TABLE `Clients` (
  `id_client` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `address` varchar(100) COLLATE utf8_bin NOT NULL,
  `phone` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `token` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Clients`
--

INSERT INTO `Clients` (`id_client`, `name`, `address`, `phone`, `email`, `password`, `token`) VALUES
(1, 'Andrey', 'Paris', '+380689809654', 'test@test.com', 'qwerty1234', 'a0de3c315ec50bc3aa6243646afae141d6fdbf7f'),
(2, 'Elena', 'Lviv', '+380939548745', 'elena@gmail.com', 'qwerty', ''),
(3, 'Andrey', 'Paris', '+380', 'asd@asd', 'f10e2821bbbea527ea02200352313bc059445190', 'b2d4dff39837285b2278f99147e22f689902f5ea'),
(4, 'Andrey', 'Paris', 'test@test.com', '+380123', '153fa238cec90e5a24b85a79109f91ebe68ca481', ''),
(21, 'asd', 'asd', '125435654654', 'asd@asda', '8e545e1c31f91f777c894b3bd2c2e7d7044cc9dd', 'f08d8fa9e3e60af6b3563d366f7dbc287066e239');

-- --------------------------------------------------------

--
-- Table structure for table `Color`
--

CREATE TABLE `Color` (
  `id_color` int(10) NOT NULL,
  `color` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Color`
--

INSERT INTO `Color` (`id_color`, `color`) VALUES
(1, 'Red'),
(2, 'Green'),
(3, 'Black'),
(4, 'Blue'),
(5, 'White'),
(6, 'Yellow'),
(7, 'Pink');

-- --------------------------------------------------------

--
-- Table structure for table `Delivery`
--

CREATE TABLE `Delivery` (
  `id_delivery` int(10) NOT NULL,
  `type` varchar(100) COLLATE utf8_bin NOT NULL,
  `price` int(50) NOT NULL,
  `status` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Delivery`
--

INSERT INTO `Delivery` (`id_delivery`, `type`, `price`, `status`) VALUES
(1, 'Nova Poshta', 0, 'Done'),
(2, 'Nova Poshta', 0, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `Material`
--

CREATE TABLE `Material` (
  `id_material` int(10) NOT NULL,
  `material` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Material`
--

INSERT INTO `Material` (`id_material`, `material`) VALUES
(1, 'Cotton 100%'),
(2, 'Cotton 95%, Polyester 10%'),
(3, 'Cotton 95%, Viscose 5%');

-- --------------------------------------------------------

--
-- Table structure for table `Options`
--

CREATE TABLE `Options` (
  `id_option` int(10) NOT NULL,
  `id_material` int(10) NOT NULL,
  `id_product` int(10) NOT NULL,
  `id_type` int(10) NOT NULL,
  `id_color` int(10) NOT NULL,
  `id_size` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Options`
--

INSERT INTO `Options` (`id_option`, `id_material`, `id_product`, `id_type`, `id_color`, `id_size`) VALUES
(69, 1, 1, 1, 1, 1),
(73, 1, 1, 1, 1, 2),
(139, 1, 1, 1, 2, 1),
(161, 1, 1, 1, 5, 2),
(162, 1, 1, 6, 5, 2),
(78, 1, 2, 1, 1, 1),
(127, 1, 2, 1, 2, 1),
(92, 1, 2, 1, 4, 1),
(150, 1, 2, 1, 5, 1),
(107, 1, 3, 1, 1, 1),
(155, 2, 1, 1, 1, 2),
(91, 2, 1, 1, 2, 1),
(164, 2, 1, 1, 2, 2),
(159, 2, 1, 5, 6, 2),
(79, 2, 2, 1, 1, 1),
(163, 2, 2, 5, 1, 1),
(82, 2, 3, 2, 4, 2),
(83, 2, 3, 2, 6, 2),
(132, 3, 2, 1, 1, 1),
(95, 3, 2, 1, 2, 1),
(81, 3, 2, 1, 4, 1),
(85, 3, 3, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE `Orders` (
  `id_order` int(10) NOT NULL,
  `id_client` int(10) NOT NULL,
  `id_payment` int(10) DEFAULT NULL,
  `id_delivery` int(10) DEFAULT NULL,
  `order_date` date NOT NULL DEFAULT current_timestamp(),
  `total_price` int(100) NOT NULL,
  `status` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Orders`
--

INSERT INTO `Orders` (`id_order`, `id_client`, `id_payment`, `id_delivery`, `order_date`, `total_price`, `status`) VALUES
(1, 1, 1, 1, '2021-04-27', 0, 'Done'),
(2, 2, 2, 1, '2021-04-28', 0, 'Pending'),
(65, 6, 1, 1, '2021-06-11', 40, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `Order_Details`
--

CREATE TABLE `Order_Details` (
  `id_order_detail` int(10) NOT NULL,
  `id_order` int(10) NOT NULL,
  `id_tshirt` int(10) NOT NULL,
  `id_option` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `price` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Order_Details`
--

INSERT INTO `Order_Details` (`id_order_detail`, `id_order`, `id_tshirt`, `id_option`, `count`, `price`) VALUES
(3, 0, 2, 2, 1, 100),
(4, 0, 2, 1, 1, 120);

-- --------------------------------------------------------

--
-- Table structure for table `Payment`
--

CREATE TABLE `Payment` (
  `id_payment` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Payment`
--

INSERT INTO `Payment` (`id_payment`, `name`) VALUES
(1, 'Visa'),
(2, 'MasterCard'),
(3, 'Offline');

-- --------------------------------------------------------

--
-- Table structure for table `Size`
--

CREATE TABLE `Size` (
  `id_size` int(10) NOT NULL,
  `size` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Size`
--

INSERT INTO `Size` (`id_size`, `size`) VALUES
(1, 'S'),
(2, 'M'),
(3, 'L'),
(4, 'XL');

-- --------------------------------------------------------

--
-- Table structure for table `Type`
--

CREATE TABLE `Type` (
  `id_type` int(10) NOT NULL,
  `type` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Type`
--

INSERT INTO `Type` (`id_type`, `type`) VALUES
(1, 'Man'),
(2, 'Unisex'),
(3, 'Woman'),
(4, 'Children'),
(5, 'Long sleeve'),
(6, 'Ringer');

-- --------------------------------------------------------

--
-- Table structure for table `T_shirt`
--

CREATE TABLE `T_shirt` (
  `id_tshirt` int(10) NOT NULL,
  `tshirt_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `price` int(50) NOT NULL,
  `available` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `T_shirt`
--

INSERT INTO `T_shirt` (`id_tshirt`, `tshirt_name`, `price`, `available`) VALUES
(1, 'DC', 20, 50),
(2, 'Marvel', 15, 100),
(3, 'Casual', 11, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Cart`
--
ALTER TABLE `Cart`
  ADD PRIMARY KEY (`id_cart`),
  ADD UNIQUE KEY `id_client` (`id_client`,`id_tshirt`,`id_option`);

--
-- Indexes for table `Category`
--
ALTER TABLE `Category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `Clients`
--
ALTER TABLE `Clients`
  ADD PRIMARY KEY (`id_client`);

--
-- Indexes for table `Color`
--
ALTER TABLE `Color`
  ADD PRIMARY KEY (`id_color`);

--
-- Indexes for table `Delivery`
--
ALTER TABLE `Delivery`
  ADD PRIMARY KEY (`id_delivery`);

--
-- Indexes for table `Material`
--
ALTER TABLE `Material`
  ADD PRIMARY KEY (`id_material`);

--
-- Indexes for table `Options`
--
ALTER TABLE `Options`
  ADD PRIMARY KEY (`id_option`),
  ADD UNIQUE KEY `id_material` (`id_material`,`id_product`,`id_type`,`id_color`,`id_size`);

--
-- Indexes for table `Orders`
--
ALTER TABLE `Orders`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `Order_Details`
--
ALTER TABLE `Order_Details`
  ADD PRIMARY KEY (`id_order_detail`),
  ADD UNIQUE KEY `id_client` (`id_order`,`id_tshirt`,`id_option`);

--
-- Indexes for table `Payment`
--
ALTER TABLE `Payment`
  ADD PRIMARY KEY (`id_payment`);

--
-- Indexes for table `Size`
--
ALTER TABLE `Size`
  ADD PRIMARY KEY (`id_size`);

--
-- Indexes for table `Type`
--
ALTER TABLE `Type`
  ADD PRIMARY KEY (`id_type`);

--
-- Indexes for table `T_shirt`
--
ALTER TABLE `T_shirt`
  ADD PRIMARY KEY (`id_tshirt`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Cart`
--
ALTER TABLE `Cart`
  MODIFY `id_cart` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=513;

--
-- AUTO_INCREMENT for table `Category`
--
ALTER TABLE `Category`
  MODIFY `id_category` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `Clients`
--
ALTER TABLE `Clients`
  MODIFY `id_client` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `Color`
--
ALTER TABLE `Color`
  MODIFY `id_color` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `Delivery`
--
ALTER TABLE `Delivery`
  MODIFY `id_delivery` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `Material`
--
ALTER TABLE `Material`
  MODIFY `id_material` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Options`
--
ALTER TABLE `Options`
  MODIFY `id_option` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `Orders`
--
ALTER TABLE `Orders`
  MODIFY `id_order` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `Order_Details`
--
ALTER TABLE `Order_Details`
  MODIFY `id_order_detail` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `Payment`
--
ALTER TABLE `Payment`
  MODIFY `id_payment` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `Size`
--
ALTER TABLE `Size`
  MODIFY `id_size` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Type`
--
ALTER TABLE `Type`
  MODIFY `id_type` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `T_shirt`
--
ALTER TABLE `T_shirt`
  MODIFY `id_tshirt` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
