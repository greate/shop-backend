const typeController = require('../controllers/typeController');
const typeRouter = require('express').Router();

typeRouter.get('/', typeController.getAllTypes);

module.exports = typeRouter;
