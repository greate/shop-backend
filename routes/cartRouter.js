const express = require('express');
const cartController = require('../controllers/cartController');
const cartRouter = require('express').Router();
const jsonParser = express.json();

cartRouter.get('/:id', cartController.getCartItemsByClientId);
cartRouter.post('/', jsonParser, cartController.addCartItem);
cartRouter.post('/deleteCart', jsonParser, cartController.deleteCartItem);

module.exports = cartRouter;
