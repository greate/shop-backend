const express = require('express');
const orderController = require('../controllers/orderController');
const orderRouter = require('express').Router();
const jsonParser = express.json();

orderRouter.post('/create', jsonParser, orderController.create);

module.exports = orderRouter;
