const categoryController = require('../controllers/categoryController');
const categoryRouter = require('express').Router();

categoryRouter.get('/:id', categoryController.getCategoriesById);

module.exports = categoryRouter;
