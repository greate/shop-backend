const express = require('express');
const userRouter = require('express').Router();
const userController = require('../controllers/userController');
const jsonParser = express.json(); 

userRouter.post('/create', jsonParser, userController.register);
userRouter.post('/login', jsonParser, userController.login);
userRouter.get('/getUser/:email', userController.getUserByEmail);

userRouter.use((req, res) => {
  res.status(404).render('pages/404.hbs');
});

module.exports = userRouter;