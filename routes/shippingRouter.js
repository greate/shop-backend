const express = require('express');
const shippingController = require('../controllers/shippingController');
const shippingRouter = require('express').Router();
const jsonParser = express.json();

shippingRouter.get('/:id', shippingController.getShippingById);
shippingRouter.post('/', jsonParser, shippingController.addShippingOrder);

module.exports = shippingRouter;
