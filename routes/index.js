const userRouter = require('./userRouter');
const categoryRouter = require('./categoryRouter');
const shippingRouter = require('./shippingRouter');
const optionsRouter = require('./optionsRouter');
const orderDetailsRouter = require('./orderDetailsRouter');
const orderRouter = require('./orderRouter');
const cartRouter = require('./cartRouter');
const colorRouter = require('./colorRouter');
const typeRouter = require('./typeRouter');
const sizeRouter = require('./sizeRouter');
const materialRouter = require('./materialRouter');
const productRouter = require('./productRouter');
const paymentRouter = require('./paymentRouter');


module.exports = {
  userRouter,
  categoryRouter,
  shippingRouter,
  optionsRouter,
  orderDetailsRouter,
  orderRouter,
  cartRouter,
  colorRouter,
  typeRouter,
  sizeRouter,
  materialRouter,
  productRouter,
  paymentRouter
};
