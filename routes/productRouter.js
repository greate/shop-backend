const productController = require('../controllers/productController');
const productRouter = require('express').Router();

productRouter.get('/', productController.getProducts);

module.exports = productRouter;
