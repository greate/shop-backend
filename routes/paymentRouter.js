const paymentController = require('../controllers/paymentController');
const paymentRouter = require('express').Router();

paymentRouter.get('/', paymentController.getPaymentsMethods);

module.exports = paymentRouter;
