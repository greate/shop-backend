const express = require('express');
const optionsController = require('../controllers/optionsController');
const optionsRouter = require('express').Router();
const jsonParser = express.json();

optionsRouter.get('/:id', optionsController.getOptionById);
optionsRouter.get('/', optionsController.getAllOptions);
optionsRouter.post('/', jsonParser, optionsController.addOption);
optionsRouter.post('/getOption', jsonParser, optionsController.getOptionId);

module.exports = optionsRouter;
