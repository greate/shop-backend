const colorController = require('../controllers/colorController');
const colorRouter = require('express').Router();

colorRouter.get('/', colorController.getColors);

module.exports = colorRouter;
