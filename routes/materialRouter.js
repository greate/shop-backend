const materialController = require('../controllers/materialController');
const materialRouter = require('express').Router();

materialRouter.get('/', materialController.getMaterials);

module.exports = materialRouter;
