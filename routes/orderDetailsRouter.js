const express = require('express');
const orderDetailsController = require('../controllers/orderDetailsController');
const orderDetailsRouter = require('express').Router();
const jsonParser = express.json();

orderDetailsRouter.get('/:id', orderDetailsController.getAllOrderDetailsById);
orderDetailsRouter.post('/', jsonParser, orderDetailsController.addOrderDetailsToTable);

module.exports = orderDetailsRouter;
