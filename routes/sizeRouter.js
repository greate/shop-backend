const sizeController = require('../controllers/sizeController');
const sizeRouter = require('express').Router();

sizeRouter.get('/', sizeController.getAllSizes);

module.exports = sizeRouter;
