const Database = require('./DB');

class OrderModel {
  createOrder(id_client, id_payment, id_delivery, total_price, callback) {
    if (!id_client)
      return callback({
        success: false,
        msg: 'id_client is missing',
      });
    if (!id_payment)
      return callback({
        success: false,
        msg: 'id_payment is missing',
      });
    if (!id_delivery)
      return callback({
        success: false,
        msg: 'id_delivery is missing',
      });
    if (!total_price)
      return callback({
        success: false,
        msg: 'total_price is missing',
      });

    Database.query(`
      insert into Orders (id_client, id_payment, id_delivery, total_price) values (?, ?, ?, ?)
    `, [id_client, id_payment, id_delivery, total_price], result => {
      callback(result);
    })
  }

  moveCartToOrder(id_order, id_client, callback) {
     Database.query(`
      insert into Order_Details (id_order, id_tshirt, id_option, count, price) 
      select ?, c.id_tshirt, c.id_option, c.count, t.price from Cart c 
      left join T_shirt t on c.id_tshirt = t.id_tshirt
      where c.id_client = ?;
    `, [id_order, id_client], result => {
      Database.query(`delete from Cart where id_client = ?;`, [id_client], result => callback(result))
    });
  }
}

module.exports = new OrderModel();
