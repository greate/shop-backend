const Database = require('../models/DB');

class ProductModel {
    getProducts(callback) {
        Database.query(`select id_tshirt as id, tshirt_name as name, price, available from T_shirt`, result => {
            callback(result);
        });
    };
}

module.exports = new ProductModel();
