const Database = require('../models/DB');

class SizeModel {
    getSizes(callback) {
        Database.query(`select id_size as id, size from Size`, result => {
            callback(result);
        });
    };
}

module.exports = new SizeModel();
