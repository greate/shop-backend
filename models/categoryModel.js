const Database = require('../models/DB');

class CategoryModel {
  getCategoryById(id, callback) {
    Database.query(`select id_category, name as category_name, tshirt_name 
    from Category 
    inner join T_shirt 
    on Category.id_category=T_shirt.id_tshirt 
    where id_category=? 
    limit 1`, [id], result => {
      callback(result);
    });
  };
}

module.exports = new CategoryModel();
