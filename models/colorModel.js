const Database = require('../models/DB');

class ColorModel {
    getColors(callback) {
        Database.query(`select id_color as id, color from Color`, result => {
            callback(result);
        });
    };
}

module.exports = new ColorModel();
