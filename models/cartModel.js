const Database = require('./DB');

class CartModel {
  isExists(id_client, id_tshirt, id_option, callback) {
    Database.query(
      'select COUNT(id_cart) as `exists` from Cart where id_client=? and id_tshirt=? and id_option=? limit 1',
      [id_client, id_tshirt, id_option],
      result => {
        callback(result.msg[0].exists === 1);
      }
    );
  }

  addCartItem(id_client, id_tshirt, id_option, count, callback) {
    Database.query(`
      insert into \`Cart\` (id_client, id_tshirt, id_option, count) values (?, ?, ?, ?) 
      on duplicate key update count = ?
    `, [id_client, id_tshirt, id_option, count, count], result => {
      callback(result)
    }
    );
  }

  deleteCartItem(id_client, id_tshirt, id_option, callback) {
    Database.query(`
    delete from Cart where id_client=? and id_tshirt=? and id_option=?
    `, [id_client, id_tshirt, id_option], result => callback(result));
  }

  getCartItemsByClientId(id_client, callback) {
    Database.query(`select t.id_tshirt as product_id, tshirt_name as name, price, available, m.id_material as material, count, type.id_type as type, s.id_size as size, color.id_color as color 
    from Cart as c 
    join T_shirt as t on c.id_tshirt=t.id_tshirt 
    join Options as o on c.id_option=o.id_option
    join Material as m on o.id_material=m.id_material
    join Type as type on o.id_type=type.id_type
    join Size as s on o.id_size=s.id_size
    join Color as color on o.id_color=color.id_color
    where id_client=?`, [id_client], result => {
      callback(result);
    });
  }
}

module.exports = new CartModel();
