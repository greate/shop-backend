const Database = require('../models/DB');

class TypeModel {
    getTypes(callback) {
        Database.query(`select id_type as id, type from Type`, result => {
            callback(result);
        });
    };
}

module.exports = new TypeModel();
