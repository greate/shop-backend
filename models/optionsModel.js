const Database = require('../models/DB');

class OptionsModel {
  getOption(id, callback) {
    Database.query(`SELECT id_option as id, material, color, tshirt_name as name, type, size, price, available FROM Options as o 
    JOIN Material as m ON o.id_material=m.id_material 
    JOIN T_shirt as t ON o.id_product=t.id_tshirt 
    JOIN Type ON o.id_type=Type.id_type
    JOIN Size as s ON o.id_size=s.id_size
    JOIN Color as c ON o.id_color=c.id_color where id_option=? limit 1`, [id], result => {
      callback(result);
    });
  }

  getIdOption(id_material, id_product, id_type, id_color, id_size, callback) {
    Database.query(`SELECT id_option as id from Options where id_material=? and id_product=? and id_type=? and id_color=? and id_size=? limit 1`,
      [id_material, id_product, id_type, id_color, id_size], result => {
        callback({
          ...result,
          msg: result.msg[0].id
        });
      });
  }

  getAllOptions(callback) {
    Database.query(`SELECT id_option as id, material, color, tshirt_name as name, type, size, price, available FROM Options as o 
    JOIN Material as m ON o.id_material=m.id_material
    JOIN T_shirt as t ON o.id_product=t.id_tshirt
    JOIN Type ON o.id_type=Type.id_type
    JOIN Size as s ON o.id_size=s.id_size
    JOIN Color as c ON o.id_color=c.id_color`, result => {
      callback(result);
    });
  }

  addOneOption(id_material, id_product, id_type, id_color, id_size, callback) {
    Database.query(
      ` insert into \`Options\` (id_material, id_product, id_type, id_color, id_size) values (?, ?, ?, ?, ?) 
     on duplicate key update id_material=?, id_product=?, id_type=?, id_color=?, id_size=?`,
      [id_material, id_product, id_type, id_color, id_size, id_material, id_product, id_type, id_color, id_size],
      result => {
        const { success, msg } = result;

        if (!success) return callback(msg);
        callback(result);
      }
    );
  }
}


module.exports = new OptionsModel();
