const Database = require('../models/DB');

class ShippingModel {
  getShippingById(id, callback) {
    Database.query('select * from Delivery where id_delivery=? limit 1', [id], result => {
      callback(result);
    });
  };

  addShipping(type, price, status, callback) {
    Database.query(
      "insert into Delivery values (NULL, ?, ?, ?)",
      [type, price, status],
      result => {
        const { success, msg } = result;

        if (!success) return callback(msg);
        callback(result);
      }
    );
  };
}

module.exports = new ShippingModel();
