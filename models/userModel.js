const Database = require('./DB');

class UserModel {
  isExists(email, callback) {
    Database.query(
      'select COUNT(id_client) as `exists` from Clients where email=? LIMIT 1',
      [email],
      result => {
        callback(result.msg[0].exists === 1);
      }
    );
  }

  register(name, address, phone, email, password, callback) {
    if (!name)
      return callback({
        success: false,
        msg: 'Name is required',
      });

    if (!address)
      return callback({
        success: false,
        msg: 'Address is required',
      });

    if (!phone)
      return callback({
        success: false,
        msg: 'Phone is required',
      });

    if (!email)
      return callback({
        success: false,
        msg: 'Email is required',
      });

    if (!password)
      return callback({
        success: false,
        msg: 'Password is required',
      });

    this.isExists(email, isExists => {
      if (isExists)
        return callback({
          success: false,
          msg: 'User already exists',
        });

      Database.query(
        "insert into Clients values (NULL, ?, ?, ?, ? , SHA1(?), '')",
        [name, address, phone, email, password],
        result => {
          const { success, msg } = result;

          if (!success) return callback(msg);

          callback(result);
        }
      );
    });
  }

  login(email, password, callback) {
    if (!email)
      return callback({
        success: false,
        msg: 'Email is required',
      });

    if (!password)
      return callback({
        success: false,
        msg: 'Password is required',
      });

    Database.query(
      'select id_client, email, password from Clients where email=? and password=SHA1(?) limit 1',
      [email, password],
      result => {
        const { success, msg } = result;

        if (success) {
          if (msg.length != 0) {
            const { id_client, password: securedPass } = msg[0];
            Database.query(
              'update Clients set token=SHA1(?) where id_client=? limit 1',
              [`${email}${securedPass}${new Date().getTime()}`, id_client],
              result => {
                const { success } = result;

                if (success) {
                  Database.query(
                    'select token FROM clients WHERE id_client=?',
                    [id],
                    result => {
                      callback({
                        success: true,
                        msg: result.msg[0].token,
                      });
                    }
                  );
                } else {
                  callback({
                    success: false,
                    msg: `Smth went wrong. Please try later.`,
                  });
                }
              }
            );
          } else {
            callback({
              success: false,
              msg: `User ${email} not found`,
            });
          }
        } else {
          callback(result);
        }
      }
    );
  }

  async loginPromised(email, password, callback) {
    if (!email)
      return callback({
        success: false,
        msg: 'Email is required',
      });

    if (!password)
      return callback({
        success: false,
        msg: 'Password is required',
      });

    try {
      const [userInfo] = await Database.promise().execute(
        'select id_client, email, password from Clients where email=? and password=SHA1(?) limit 1',
        [email, password]
      );

      if (userInfo.length === 0)
        return callback({ success: false, msg: 'User not exists' });

      const { password: securedPass, id_client } = userInfo[0];

      const [{ affectedRows }] = await Database.promise().execute(
        'update Clients set token=SHA1(?) where id_client=? limit 1',
        [`${email}${securedPass}${new Date().getTime()}`, id_client]
      );


      if (affectedRows === 0)
        return callback({
          success: false,
          msg: 'Smth went wrong. Please try later.',
        });

      const [userToken] = await Database.promise().execute(
        'select token from Clients where id_client=? limit 1',
        [id_client]
      );

      if (userToken.length === 0)
        return callback({
          success: false,
          msg: 'Smth went wrong. Please try later.',
        });

      callback({ success: true, msg: userToken[0].token });
    } catch (error) {
      callback({ success: false, msg: JSON.stringify(error) });
    }
  }

  getUserByEmail(email, callback) {
    Database.query(
      'select id_client from Clients where email=? limit 1',
      [email],
      result => {
        callback({
          ...result,
          msg: result.msg[0].id_client
        });
      }
    );
  }
}

module.exports = new UserModel();