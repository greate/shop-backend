const Database = require('../models/DB');

class OrderDetailsModel {
  getAllOrderDetailsById(id, callback) {
    Database.query('select * from Order_Details where id_order_detail=? limit 1', [id], result => {
      callback(result);
    });
  };

  addOrderDetails(id_client, id_tshirt, id_option, count, price, callback) {
    Database.query(`
    insert into \`Order_Details\` (id_client, id_tshirt, id_option, count, price) values (?, ?, ?, ?, ?) 
    on duplicate key update count = ? and price = ?
  `, [id_client, id_tshirt, id_option, count, price, count, price], result => {
      callback(result)
    }
    );
  };
}

module.exports = new OrderDetailsModel();
